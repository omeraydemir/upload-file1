from django import forms
from .models import *
from .views import *



class CloudForm(forms.ModelForm):
    class Meta:
        model = Cloud
        fields = ('title', 'file' , 'id' , 'version')
