from django.shortcuts import render , HttpResponse ,get_object_or_404 , redirect ,render_to_response , render
from .models import *
from .forms import CloudForm
from .apps import *
from .migrations import *
import os , re
from django.db.models import F
from django.core.exceptions import ValidationError


# Create your views here.
def upload_file(request):
    if request.method == 'POST':
        print(request.POST)
        form = CloudForm(request.POST, request.FILES)
        if form.is_valid():
            form.save()
            return redirect('/home')
    else:
        form = CloudForm()
    return render(request, 'cloud/upload.html', {
        'form': form
    })




def show(request):
    apps = CloudForm.objects.all()
    if "version" == "3.20"
    else:
        return redirect

    context = {
        'apps' : apps,
    }
    return render(request, 'cloud/show.html' , {'apps' : apps} , context)


""""
def validate_is_pdf(file):
    valid_mime_types = ['application/pdf']
    file_mime_type = magic.from_buffer(file.read(1024), mime=True)
    if file_mime_type not in valid_mime_types:
        raise ValidationError('Unsupported file type.')
    valid_file_extensions = ['.pdf']
    ext = os.path.splitext(file.name)[1]
    if ext.lower() not in valid_file_extensions:
        raise ValidationError('Unacceptable file extension.')

#----------------------------------------------------------------

def version(path_string):
    STATIC_PATH = "/path/to/templates/"
    version_cache = {}

    rx = re.compile(r"^(.*)\.(.*?)$")
    try:
        if path_string in version_cache:
            mtime = version_cache[path_string]
        else:
            mtime = os.path.getmtime('%s%s' % (STATIC_PATH, path_string,))
            version_cache[path_string] = mtime

        return rx.sub(r"\1.%d.\2" % mtime, path_string)
    except:
        return path_string


    register.simple_tag(version)
