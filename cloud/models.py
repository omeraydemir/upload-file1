from django.db import models

# Create your models here.

#from myapp.validators import validate_is_pdf


class Cloud(models.Model):

    title = models.CharField(max_length=255, blank=True , verbose_name='Title')
    file = models.FileField()
    uploaded_at = models.DateTimeField(auto_now_add=True)
    version = models.CharField(max_length=255,blank=True,verbose_name='Versiyon')

    def __str__(self):
        return self.title



    def get_absolute_url(self):
        return "/cloud/{}".format(self.id)




#class PdfFile(models.Model):
    #file = models.FileField(upload_to='pdfs/', validators=(validate_is_pdf,))