from django.contrib import admin
from .models import *
from .views import *
# Register your models here.



class FileAdmin(admin.ModelAdmin):
    list_display = ['title', 'file' , 'id' , 'uploaded_at']
    list_display_links = ['title']
    list_filter = ['uploaded_at']
    search_fields = ['title' , 'uploaded_at']
    list_editable = ['file']
    class Meta:
        model = Cloud


admin.site.register(Cloud,FileAdmin)


# Register your models here.
