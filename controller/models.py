from django.db import models


# Create your models here.


class Controller(models.Model):
    title = models.CharField(max_length=255, blank=True, verbose_name='Title')
    file = models.FileField()
    uploaded_at = models.DateTimeField(auto_now_add=True)
    version = models.CharField(max_length=255, blank=True, verbose_name='Versiyon')

    def __str__(self):
        return self.title


    def get_absolute_url(self):
        return "/controller/{}".format(self.id)

from django.db import models

# Create your models here.
