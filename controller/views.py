from django.shortcuts import render , HttpResponse ,get_object_or_404 , redirect ,render_to_response , render
from .models import *
from .forms import ControllerForm
from .apps import *
from .migrations import *
import os
from django.db.models import F



# Create your views here.
def upload_file(request):
    if request.method == 'POST':
        print(request.POST)
        form = ControllerForm(request.POST, request.FILES)
        if form.is_valid():
            form.save()
            return redirect('/home')
    else:
        form = ControllerForm()
    return render(request, 'controller/upload.html', {
        'form': form
    })




def show(request):
    apps = Controller.objects.all()


    context = {
        'apps' : apps,
    }
    return render(request, 'controller/show.html' , {'apps' : apps} , context)


