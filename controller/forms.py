from django import forms
from .models import *
from .views import *



class ControllerForm(forms.ModelForm):
    class Meta:
        model = Controller
        fields = ('title', 'file' , 'id' , 'version')
